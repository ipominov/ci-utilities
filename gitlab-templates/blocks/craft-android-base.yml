.craft_android_base:
  stage: deploy
  rules:
    # run the job on mainline branches of mainline repositories
    - if: $KDECI_SECURE_SERVICES_KEY
      when: on_success
    # or when triggered manually
    - when: manual
      # we don't want the manual job to block the pipeline
      allow_failure: true
  tags:
    - Linux
  variables:
    GIT_STRATEGY: none
    # KDECI_CRAFT_PLATFORM is specified in the derived jobs
    KDECI_CRAFT_CACHE: /mnt/craft-cache/$KDECI_CRAFT_PLATFORM/
    # KDECI_CRAFT_CONFIG is specified in the derived jobs
    KDECI_CRAFT_PROJECT_CONFIG: $CI_PROJECT_DIR/src/.craft.ini
    KDECI_SIGNAPK_CONFIG: $CI_PROJECT_DIR/ci-utilities/signing/signapk.ini
  interruptible: true
  before_script:
    - git clone https://invent.kde.org/packaging/craftmaster.git --branch=master
    - git clone https://invent.kde.org/sysadmin/ci-utilities.git --depth=1
    - git clone https://invent.kde.org/sysadmin/ci-notary-service.git --depth=1
    - python3 -u ci-utilities/gitlab-ci-clone.py src/
    # Create empty .craft.ini if none exists
    - touch $KDECI_CRAFT_PROJECT_CONFIG
    # Define a short cut for the lengthy CraftMaster command line
    - function craftmaster { python3 craftmaster/CraftMaster.py --config $KDECI_CRAFT_CONFIG --config-override $KDECI_CRAFT_PROJECT_CONFIG --target $KDECI_CRAFT_PLATFORM $@; }
  script:
    # Set up craft settings and blueprint settings
    - craftmaster --setup
    # Get Craft itself ready
    - craftmaster -c -i craft
    # Install all of our dependencies
    - craftmaster -c --install-deps $CI_PROJECT_NAME
    # Build the actual application
    - craftmaster -c --no-cache --options $CI_PROJECT_NAME.srcDir=$CI_PROJECT_DIR/src/ $CI_PROJECT_NAME
    # Package it up!
    - craftmaster -c --package --options $CI_PROJECT_NAME.srcDir=$CI_PROJECT_DIR/src/ $CI_PROJECT_NAME
    # Save our package
    - packageDir=$(craftmaster -c -q --get "packageDestinationDir()" virtual/base)
    - mkdir $CI_PROJECT_DIR/kde-ci-packages/
    - cp -vrf $packageDir/*.apk $packageDir/metadata/*.zip $CI_PROJECT_DIR/kde-ci-packages/
    # Sign the APK (if the project branch is cleared for signing)
    - python3 ci-notary-service/signapk.py -v --config $KDECI_SIGNAPK_CONFIG $CI_PROJECT_DIR/kde-ci-packages/*.apk
    # Archive the artifacts we need for creating an application bundle
    - cd $CI_PROJECT_DIR
    # Get the relative path of the build directory and the archive directory
    - buildDir=$(craftmaster -c -q --get "buildDir()" --options $CI_PROJECT_NAME.srcDir=$CI_PROJECT_DIR/src/ $CI_PROJECT_NAME)
    - buildDir=${buildDir#$(pwd)/}
    - archiveDir=$(craftmaster -c -q --get "archiveDir()" --options $CI_PROJECT_NAME.srcDir=$CI_PROJECT_DIR/src/ $CI_PROJECT_NAME)
    - archiveDir=${archiveDir#$(pwd)/}
    # Get the Android target (we assume a single target)
    - androidTarget=$(craftmaster -c -q --get "androidApkTargets" --options $CI_PROJECT_NAME.srcDir=$CI_PROJECT_DIR/src/ $CI_PROJECT_NAME | cut -d "'" -f 2)
    - buildApkDir=${buildDir}/${androidTarget}_build_apk
    - mkdir app-bundle-artifacts
    # Archive the architecture-specific artifacts (relative to the build_apk directory)
    - (cd ${buildApkDir} && tar czf ${CI_PROJECT_DIR}/app-bundle-artifacts/${KDECI_CRAFT_PLATFORM}.tar.gz libs)
    # Remove build artifacts that we do not want to archive
    - rm -rf ${buildApkDir}/{build,libs,.gradle}
    # Archive the architecture-independent artifacts (only for android-arm64-clang)
    # * The Qt Android bindings classes (for Qt 5)
    - echo ${KDECI_CRAFT_PLATFORM}/src/android/java >.files_to_archive
    # * The Qt Android bindings classes (for Qt 6)
    - if [ -d ${archiveDir}/src/android/java ]; then echo ${archiveDir}/src/android/java >>.files_to_archive; fi
    # * Everything in the build folder
    - echo ${buildApkDir} >>.files_to_archive
    # * The version.gradle file (if it exists)
    - if [ -f ${buildDir}/version.gradle ]; then echo ${buildDir}/version.gradle >>.files_to_archive; fi
    - if [ ${KDECI_CRAFT_PLATFORM} = "android-arm64-clang" ]; then tar czf app-bundle-artifacts/common.tar.gz --exclude-backups --files-from .files_to_archive; fi
    # Remember the build_apk path for subsequent jobs
    - echo "${CI_PROJECT_DIR}/${buildApkDir}" >app-bundle-artifacts/build-apk-path-${KDECI_CRAFT_PLATFORM}

.craft_android_appbundle:
  stage: deploy
  rules:
    # run the job on mainline branches of mainline repositories
    - if: $KDECI_SECURE_SERVICES_KEY
      when: on_success
    # or when triggered manually
    - when: manual
      # we don't want the manual job to block the pipeline
      allow_failure: true
  tags:
    - Linux
  variables:
    GIT_STRATEGY: none
  interruptible: true
  script:
    # Set APK_PATH to the path of the craft_android_arm64 job (where we captured the architecture-independent artifacts)
    - export APK_PATH=$(cat app-bundle-artifacts/build-apk-path-android-arm64-clang)
    # Unpack the artifacts
    - tar xzf app-bundle-artifacts/common.tar.gz
    - cd ${APK_PATH}
    - tar xzf ${CI_PROJECT_DIR}/app-bundle-artifacts/android-arm-clang.tar.gz
    - tar xzf ${CI_PROJECT_DIR}/app-bundle-artifacts/android-arm64-clang.tar.gz
    - tar xzf ${CI_PROJECT_DIR}/app-bundle-artifacts/android-x86_64-clang.tar.gz
    # Now build the app bundle
    - ./gradlew bundleRelease
    - buildAabName=$(basename $(ls build/outputs/bundle/release/*_build_apk-release.aab))
    - finalAabName=${buildAabName%_build_apk-release.aab}-${CI_COMMIT_REF_SLUG}.aab
    - mv -vf build/outputs/bundle/release/${buildAabName} ${CI_PROJECT_DIR}/${finalAabName}
    # Move the fastlane metadata file next to the app bundle file
    - mv $CI_PROJECT_DIR/kde-ci-packages/fastlane-*.zip ${CI_PROJECT_DIR}
  artifacts:
    expire_in: 3 days
    when: on_success
    paths:
      - "*.aab"
      - "fastlane-*.zip"

.android_publish:
  stage: publish
  rules:
    # add the job to pipelines on mainline branches of mainline repositories
    - if: $KDECI_SECURE_SERVICES_KEY
  tags:
    - Linux
  variables:
    GIT_STRATEGY: none
  interruptible: true
  before_script:
    - git clone https://invent.kde.org/sysadmin/ci-utilities.git --depth=1
    - git clone https://invent.kde.org/sysadmin/ci-notary-service.git --depth=1
  artifacts:
    expire_in: 3 days
    when: always
    paths:
      - "kde-ci-packages/task*.log"

.fdroid_apks:
  extends:
    .android_publish
  variables:
    KDECI_PUBLISHONFDROID_CONFIG: $CI_PROJECT_DIR/ci-utilities/signing/publishonfdroid.ini
  script:
    # Publish the APKs (if the project branch is cleared for publishing)
    - python3 ci-notary-service/publishonfdroid.py -v --config $KDECI_PUBLISHONFDROID_CONFIG --fastlane $CI_PROJECT_DIR/kde-ci-packages/fastlane-*.zip $CI_PROJECT_DIR/kde-ci-packages/*.apk

.googleplay_apks:
  extends:
    .android_publish
  variables:
    KDECI_PUBLISHONGOOGLEPLAY_CONFIG: $CI_PROJECT_DIR/ci-utilities/signing/publishongoogleplay.ini
  script:
    # Publish the APKs (if the project branch is cleared for publishing)
    - python3 ci-notary-service/publishongoogleplay.py -v --config $KDECI_PUBLISHONGOOGLEPLAY_CONFIG --fastlane $CI_PROJECT_DIR/kde-ci-packages/fastlane-*.zip $CI_PROJECT_DIR/kde-ci-packages/*.apk

.googleplay_aab:
  extends:
    .android_publish
  variables:
    KDECI_PUBLISHONGOOGLEPLAY_CONFIG: $CI_PROJECT_DIR/ci-utilities/signing/publishongoogleplay.ini
  script:
    # Publish the AAB (if the project branch is cleared for publishing)
    - python3 ci-notary-service/publishongoogleplay.py -v --config $KDECI_PUBLISHONGOOGLEPLAY_CONFIG --fastlane $CI_PROJECT_DIR/fastlane-*.zip $CI_PROJECT_DIR/*.aab

.sign_aab:
  extends:
    .android_publish
  rules:
    # add the job to pipelines on mainline branches of mainline repositories
    - if: $KDECI_SECURE_SERVICES_KEY
      when: manual
      # we don't want the manual job to block the pipeline
      allow_failure: true
  variables:
    KDECI_SIGNAAB_CONFIG: $CI_PROJECT_DIR/ci-utilities/signing/signaab.ini
  script:
    # Sign the AAB (if the project branch is cleared for signing)
    - python3 ci-notary-service/signaab.py -v --config $KDECI_SIGNAAB_CONFIG $CI_PROJECT_DIR/*.aab
  artifacts:
    expire_in: 3 days
    when: always
    paths:
      - "*.aab"
      - "kde-ci-packages/task*.log"
